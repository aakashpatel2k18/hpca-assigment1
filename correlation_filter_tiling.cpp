#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <ctime>
#include <fstream>
using namespace std;

#define FILTER_SIZE 3
#define FILTERS 2

// Optimize this function


void applyFilters(int *image, int height, int width, int *filter, int filters)
{
    for(int i = 0; i < filters; ++i)
    {
        int *copy = new int[height * width];
        for(int p1=0;p1<width;p1+=1024){
			
			
				for(int w=0; w<height;w++){
					for(int h=p1;h<p1+1024 && h<width;h++){
					
						copy[w * width + h] = 0;
                		for(int offy = -1; offy <= 1; ++offy){
                			
                    		for(int offx = -1; offx <= 1; ++offx){
                        		if(offx + h < 0 || offx + h >= height ||offy + w < 0 || offy + w >= width)
                            		continue;
                        		copy[w * width + h] += image[(offy + w) * width + offx + h] * 
                            	filter[i * FILTER_SIZE * FILTER_SIZE + (1 + offy) * FILTER_SIZE + 1 + offx];
                            }
                        }
	                }
	
                }
            
        }
         
        
	for(int w = 0; w < width; ++w)
        {
            for(int h = 0; h < height; ++h)
                image[w * width + h] = copy[w * width + h];
        }
        delete copy;
    }


}

int main()
{
    int ht, wd;
    cin >> ht >> wd;
    int *img = new int[ht * wd];
    for(int i = 0; i < ht; ++i)
        for(int j = 0; j < wd; ++j)
            cin >> img[i * wd + j];
            
    int filters = FILTERS;
    int *filter = new int[filters * FILTER_SIZE * FILTER_SIZE];
    for(int i = 0; i < filters; ++i)
        for(int j = 0; j < FILTER_SIZE; ++j)
            for(int k = 0; k < FILTER_SIZE; ++k)
                cin >> filter[i * FILTER_SIZE * FILTER_SIZE + j * FILTER_SIZE + k];
    
    clock_t begin = clock();
    applyFilters(img, ht, wd, filter, filters);
    clock_t end = clock();
    cout << "Execution time: " << double(end - begin) / (double)CLOCKS_PER_SEC << " seconds\n";
    ofstream fout("output_2.txt");
    for(int i = 0; i < ht; ++i)
    {
        for(int j = 0; j < wd; ++j)
            fout << img[i * wd + j] << " ";
        fout << "\n";
    }
}