#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <ctime>
#include <fstream>
using namespace std;

#define FILTER_SIZE 3
#define FILTERS 2

// Optimize this function
void applyFilters(int *image, int height, int width, int *filter, int filters)
{
    for(int i = 0; i < filters; ++i)
    {
        int *copy = new int[height * width];
        int temp=0;
        int temp2 = i*FILTER_SIZE*FILTER_SIZE;
        for(int w = 0; w < width; ++w)
        {
            for(int h = 0; h < height; ++h)
            {
                copy[temp] = 0;
                for(int offy = -1; offy <= 1; ++offy)
                {   
                    if(offy + w < 0 || offy + w >= width)continue;
                    for(int offx = -1; offx <= 1; ++offx)
                    {
                        if(offx + h < 0 || offx + h >= height)
                            continue;
                        copy[temp] += image[(offy + w) * width + offx + h] * 
                            filter[temp2 + (1 + offy) * FILTER_SIZE + 1 + offx];
                    }
                }
                temp++;
            }
        }
      temp = 0;  
	for(int w = 0; w < width; ++w)
        {
            for(int h = 0; h < height; ++h){
                image[temp] = copy[temp];
                temp++;
	        }
        }
        delete copy;
    }
}

int main()
{
    int ht, wd;
    cin >> ht >> wd;
    int *img = new int[ht * wd];
    for(int i = 0; i < ht; ++i)
        for(int j = 0; j < wd; ++j)
            cin >> img[i * wd + j];
            
    int filters = FILTERS;
    int *filter = new int[filters * FILTER_SIZE * FILTER_SIZE];
    for(int i = 0; i < filters; ++i)
        for(int j = 0; j < FILTER_SIZE; ++j)
            for(int k = 0; k < FILTER_SIZE; ++k)
                cin >> filter[i * FILTER_SIZE * FILTER_SIZE + j * FILTER_SIZE + k];
    
    clock_t begin = clock();
    applyFilters(img, ht, wd, filter, filters);
    clock_t end = clock();
    cout << "Execution time: " << double(end - begin) / (double)CLOCKS_PER_SEC << " seconds\n";
    ofstream fout("output_3.txt");
    for(int i = 0; i < ht; ++i)
    {
        for(int j = 0; j < wd; ++j)
            fout << img[i * wd + j] << " ";
        fout << "\n";
    }
}
